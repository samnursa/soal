<H1> Petunjuk : </H1>

MOHON DIBACA DAN DIPERHATIKAN UNTUK SEMUA CASE DIBAWAH INI

1. Terdapat API public sangat sederhana 
   Buka https://github.com/HackerNews/API

2. Bahasa yang digunakan menggunakan KOTLIN 
    
3. Tugas kalian adalah buatlah list dan detail list TOP STORY yang harus memenuhi kriteria berikut :
	- Gunakan architectural pattern (MVVM/MVI)
	- Gunakan Retrofit
	- Gunakan Room
	- Optimalkan dan gunakan teknologi teknologi terbaru di android
	
4. Tampilan UI dibebaskan

5. Waktu pengerjaan default adalah 6 Jam, selesai atau tidak commit di Git Repository

6. Jika ada yang ingin ditanyakan, silahkan kontak ke nomor 082116150754 (Sam) / 085747397223 (Egi)
